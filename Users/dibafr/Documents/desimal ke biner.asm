

;Firdaus Diba Fahrezi
;Nim 2000018103
;Kelas B
name "Konversi Bilangan Desimal ke Biner"

org 100h

jmp start

result db 16 dup('x'), 'b'
msg1 db "Nilai yang mendukung -32768 hingga 65535", 0Dh,0Ah
     db "Masukkan Angka Desimal: $"

msg2 db 0Dh,0Ah, "Konversi ke Biner: $"

start:
mov dx, offset msg1
mov ah, 9
int 21h
call scan_num 
mov bx, cx
call convert_to_bin 
mov dx, offset msg2
mov ah, 9
int 21h
mov si, offset result  
mov ah, 0eh            
mov cx, 17             
print_me:
	mov al, [si]
	int 10h 
	inc si
loop print_me
mov ah, 0
int 16h
ret 
convert_to_bin    proc     near
pusha

lea di, result

mov cx, 16
print: mov ah, 2   
       mov [di], '0'
       test bx, 1000_0000_0000_0000b  
       jz zero
       mov [di], '1'
zero:  shl bx, 1
       inc di
loop print

popa
ret
convert_to_bin   endp

putc    macro   char
        push    ax
        mov     al, char
        mov     ah, 0eh
        int     10h     
        pop     ax
endm

scan_num        proc    near
        push    dx
        push    ax
        push    si        
        mov     cx, 0
        mov     cs:make_minus, 0

next_digit:
        mov     ah, 00h
        int     16h
        mov     ah, 0eh
        int     10h      
        cmp     al, '-'
        je      set_minus        
        cmp     al, 13  
        jne     not_cr
        jmp     stop_input
not_cr:
        cmp     al, 8                
        jne     backspace_checked
        mov     dx, 0                 
        mov     ax, cx                  
        div     cs:ten                  
        mov     cx, ax
        putc    ' '                    
        putc    8                       
        jmp     next_digit
backspace_checked:
        cmp     al, '0'
        jae     ok_ae_0
        jmp     remove_not_digit
ok_ae_0:        
        cmp     al, '9'
        jbe     ok_digit
remove_not_digit:       
        putc    8      
        putc    ' '     
        putc    8       
        jmp     next_digit        
ok_digit:       
        push    ax
        mov     ax, cx
        mul     cs:ten                  
        mov     cx, ax
        pop     ax

        cmp     dx, 0
        jne     too_big

        sub     al, 30h

        mov     ah, 0
        mov     dx, cx     
        add     cx, ax
        jc      too_big2    

        jmp     next_digit

set_minus:
        mov     cs:make_minus, 1
        jmp     

too_big2:
        mov     cx, dx     
        mov     dx, 0       
too_big:
        mov     ax, cx
        div     cs:ten  
        mov     cx, ax
        putc    8      
        putc    ' '     
        putc    8              
        jmp    
        
        
stop_input:
        
        cmp     cs:make_minus, 0
        je      not_minus
        neg     cx
not_minus:

        pop     si
        pop     ax
        pop     dx
        ret
make_minus      db      ?      
ten             dw      10      
scan_num        endp

